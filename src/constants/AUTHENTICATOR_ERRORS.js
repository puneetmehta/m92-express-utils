'use strict'

import ERROR_CLASSIFICATIONS from './ERROR_CLASSIFICATIONS'

const AUTHENTICATOR_ERRORS = {
  TOKEN_NOT_FOUND: {
    statusCode: 401,
    message: 'Authentication Token Not Found',
    classification: ERROR_CLASSIFICATIONS.AUTHENTICATION
  },
  API_KEY_NOT_FOUND: {
    statusCode: 401,
    message: 'API Key Not Found',
    classification: ERROR_CLASSIFICATIONS.AUTHENTICATION
  }
}

export default AUTHENTICATOR_ERRORS
