'use strict'

const CODE_FLOW_LOG = 'CODE_FLOW'
const REQ_RES_LOG = 'REQ_RES_LOG'
const LEAN_MAP = {
  [CODE_FLOW_LOG]: 'cf',
  [REQ_RES_LOG]: 'rr'
}

const LOG_TYPES = {
  CODE_FLOW_LOG,
  REQ_RES_LOG,
  LEAN_MAP
}

export default LOG_TYPES
