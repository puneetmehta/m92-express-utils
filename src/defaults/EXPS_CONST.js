'use strict'

const ACCESS_TOKEN_HEADER_KEY = 'X-Access-Token'
const ACCESS_TOKEN_PROP = 'accessToken'

const POOL_ID_HEADER_KEY = 'X-Pool-ID'
const POOL_ID_REQUEST_KEY = 'poolId'

const UID_HEADER_KEY = 'X-App-UID'
const UID_REQUEST_KEY = 'appUid'
const UID_CONTEXT = 'appUid'

const CLAIMS_REQUEST_KEY = 'user'

const SESSION_ID_CONTEXT = 'session-id'
const SESSION_ID_HEADER_KEY = 'X-Session-ID'
const SESSION_ID_PROP = 'sessionId'

const REQUEST_ID_CONTEXT = 'request-id'
const REQUEST_ID_HEADER_KEY = 'X-Request-ID'
const REQUEST_ID_PROP = 'requestId'

const AUTH_RESPONSE_PROP = 'authBody'

const REQUEST_TOKEN_CONTEXT = 'request-token'
const RESPONSE_TOKEN_CONTEXT = 'response-token'
const USER_CONTEXT = 'user'

const API_KEY_HEADER_KEY = 'X-API-Key'
const API_KEY_QUERY_KEY = 'apiKey'
const API_KEY_REQUEST_KEY = 'apiKey'
const API_USER_CONTEXT = 'api-user'

const REQ_RES_BODY_LOG_CONTEXT = 'req-res-body-log'

const IP_ADDRESS_HEADER_KEY = 'X-Forwarded-For'
const IP_ADDRESS_REQUEST_KEY = 'ipAddress'

// These Props help Enable or Disable Default Auth
const DEFAULT_AUTH = false
const AUTH_HEADER = {
  HEADER_KEY: 'Authorization',
  QUERY_KEY: 'token',
  REQUEST_PROP: 'token'
}

const EXTRACT_CUSTOM_HEADERS = [{
  HEADER_KEY: UID_HEADER_KEY,
  REQUEST_PROP: UID_REQUEST_KEY
}, {
  HEADER_KEY: POOL_ID_HEADER_KEY,
  REQUEST_PROP: POOL_ID_REQUEST_KEY
}, {
  HEADER_KEY: ACCESS_TOKEN_HEADER_KEY,
  REQUEST_PROP: ACCESS_TOKEN_PROP
}, {
  HEADER_KEY: API_KEY_HEADER_KEY,
  QUERY_KEY: API_KEY_QUERY_KEY,
  REQUEST_PROP: API_KEY_REQUEST_KEY
}, {
  HEADER_KEY: IP_ADDRESS_HEADER_KEY,
  REQUEST_PROP: IP_ADDRESS_REQUEST_KEY
}]

const EXPOSE_HEADERS = {
  HEADER_KEY: 'Access-Control-Expose-Headers',
  HEADER_VALUE: [ACCESS_TOKEN_HEADER_KEY, SESSION_ID_HEADER_KEY, REQUEST_ID_HEADER_KEY]
}

const SET_CUSTOM_HEADERS = [{
  HEADER_KEY: ACCESS_TOKEN_HEADER_KEY,
  RESPONSE_PROP: ACCESS_TOKEN_PROP
}, {
  HEADER_KEY: SESSION_ID_HEADER_KEY,
  RESPONSE_PROP: SESSION_ID_PROP
}, {
  HEADER_KEY: REQUEST_ID_HEADER_KEY,
  RESPONSE_PROP: REQUEST_ID_PROP
}]

const DEFAULT_SOAP_VERSION = '1.2'

const EXPS_CONST = {
  DEFAULT_AUTH,
  AUTH_HEADER,
  EXPOSE_HEADERS,
  EXTRACT_CUSTOM_HEADERS,
  SET_CUSTOM_HEADERS,
  ACCESS_TOKEN_HEADER_KEY,
  ACCESS_TOKEN_PROP,
  CLAIMS_REQUEST_KEY,
  SESSION_ID_CONTEXT,
  SESSION_ID_HEADER_KEY,
  SESSION_ID_PROP,
  REQUEST_ID_CONTEXT,
  REQUEST_ID_HEADER_KEY,
  REQUEST_ID_PROP,
  AUTH_RESPONSE_PROP,
  REQUEST_TOKEN_CONTEXT,
  RESPONSE_TOKEN_CONTEXT,
  USER_CONTEXT,
  API_KEY_HEADER_KEY,
  API_KEY_QUERY_KEY,
  API_KEY_REQUEST_KEY,
  API_USER_CONTEXT,
  UID_HEADER_KEY,
  UID_REQUEST_KEY,
  UID_CONTEXT,
  DEFAULT_SOAP_VERSION,
  REQ_RES_BODY_LOG_CONTEXT
}

export default EXPS_CONST
