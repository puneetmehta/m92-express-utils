'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _expressHttpContext = _interopRequireDefault(require("express-http-context"));

var _nanoid = require("nanoid");

var _uuid = require("uuid");

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _expressValidator = require("express-validator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

class Middleware {
  constructor(CONFIG, CONSTANTS, thisExpressUtils) {
    var {
      logger,
      CustomError,
      reqHandler,
      httpContext
    } = thisExpressUtils;
    var {
      asyncWrapper
    } = reqHandler;
    this.CONFIG = CONFIG;
    this.CONSTANTS = CONSTANTS;
    this.logger = logger;
    this.CustomError = CustomError;
    this.asyncWrapper = asyncWrapper;
    this.httpContext = httpContext;
    (0, _autoBind.default)(this);
  }

  init(app) {
    var {
      CONFIG
    } = this;
    app.use(_expressHttpContext.default.middleware);
    app.use(this.apiTagging);

    if (CONFIG.LOGGER_REQ_RES_LOG_ENABLED) {
      app.use(this.apiLogger);
    }
  }

  apiTagging(request, response, next) {
    var {
      CONFIG,
      CONSTANTS,
      httpContext
    } = this;

    _tagSessionId(CONFIG, CONSTANTS, request, response, httpContext);

    _tagRequestId(CONFIG, CONSTANTS, request, response, httpContext);

    process.nextTick(next);
  }

  apiLogger(request, response, next) {
    var {
      logger,
      httpContext
    } = this;
    var {
      ReqResLog
    } = logger;
    httpContext.setReqResBodyLog(true);
    request.timestamp = Date.now();
    response.on('finish', () => {
      var reqResLog = new ReqResLog(request, response);

      logger._dispatch(reqResLog);
    });
    process.nextTick(next);
  }

  requestValidator(validationFunction, errorMap) {
    var {
      CustomError,
      asyncWrapper
    } = this;

    var validate = /*#__PURE__*/function () {
      var _ref = _asyncToGenerator(function* (request, response, next) {
        var validations = validationFunction();
        yield Promise.all(validations.map(validation => validation.run(request)));
        var errors = (0, _expressValidator.validationResult)(request);

        if (errors.isEmpty()) {
          return next();
        }

        var extractedErrors = errors.array().reduce((output, error) => {
          var {
            param,
            msg
          } = error;
          var currentValue = output[param];

          if (!currentValue) {
            output[param] = [];
          }

          output[param].push(msg);
          return output;
        }, {});
        throw new CustomError(extractedErrors, errorMap);
      });

      return function validate(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }();

    return asyncWrapper(validate);
  }

  disableReqResBodyLogging(request, response, next) {
    var {
      httpContext
    } = this;
    httpContext.setReqResBodyLog(false);
    process.nextTick(next);
  }

} // --------- API Tagging -------------------------------------------------------


exports.default = Middleware;

function _extractFromHeader() {
  var headers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var value = headers[key] || '';
  value = value || headers[key.toLowerCase()] || '';
  return value;
}

function _tagSessionId(CONFIG, CONSTANTS, request, response, httpContext) {
  var {
    SESSION_ID_HEADER_KEY,
    SESSION_ID_PROP
  } = CONSTANTS;
  var {
    headers = {}
  } = request;
  var sessionId = _extractFromHeader(headers, SESSION_ID_HEADER_KEY) || httpContext.getSessionId() || (0, _nanoid.nanoid)();
  httpContext.setSessionId(sessionId);
  request[SESSION_ID_PROP] = sessionId;
  response[SESSION_ID_PROP] = sessionId;
}

function _tagRequestId(CONFIG, CONSTANTS, request, response, httpContext) {
  var {
    REQUEST_ID_HEADER_KEY,
    REQUEST_ID_PROP
  } = CONSTANTS;
  var {
    headers = {}
  } = request;
  var sessionId = _extractFromHeader(headers, REQUEST_ID_HEADER_KEY) || httpContext.getRequestId() || (0, _uuid.v4)();
  httpContext.setRequestId(sessionId);
  request[REQUEST_ID_PROP] = sessionId;
  response[REQUEST_ID_PROP] = sessionId;
} // -----------------------------------------------------------------------------