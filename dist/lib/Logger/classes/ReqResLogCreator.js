'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ReqResLogCreator;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _LOGGER = _interopRequireDefault(require("../constants/LOGGER"));

var _LOG_TYPES = _interopRequireDefault(require("../constants/LOG_TYPES"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ReqResLogCreator(BaseLog, thisExpressUtils) {
  var {
    CONFIG
  } = BaseLog;
  var {
    LOGGER_REQ_RES_LOG_BODY_ENABLED
  } = CONFIG;
  var {
    httpContext
  } = thisExpressUtils;
  return class ReqResLog extends BaseLog {
    constructor(request, response) {
      var level = _LOGGER.default.LEVELS.TRACE;
      super(_LOG_TYPES.default.REQ_RES_LOG, level);
      (0, _autoBind.default)(this);
      var {
        sessionId,
        requestId
      } = this;
      var reqProps = this.getRequestProps(request);
      var resProps = this.getResponseProps(request, response);
      var {
        method,
        url
      } = reqProps;
      var {
        statusCode,
        status,
        responseMessage,
        responseTimeInMS
      } = resProps;
      var message = "[".concat(sessionId, ", ").concat(requestId, "] ").concat(method, " ").concat(url, " | ").concat(statusCode, " ").concat(status, " | ").concat(responseMessage, " | ").concat(responseTimeInMS, "ms");
      Object.assign(this, {
        sessionId,
        requestId,
        message
      }, reqProps, resProps);
    }

    getRequestProps(request) {
      var CONFIG = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var {
        url,
        originalUrl,
        method,
        timestamp,
        httpVersionMajor,
        httpVersionMinor,
        ipAddress,
        _remoteAddress,
        connection,
        headers,
        body
      } = request;
      var requestUrl = originalUrl || url;
      var httpVersion = "".concat(httpVersionMajor, ".").concat(httpVersionMinor);
      var requestIp = ipAddress || _remoteAddress || connection && connection.remoteAddress || undefined;
      var userAgent = headers['user-agent'];
      var reqResBodyLog = httpContext.getReqResBodyLog();
      var reqBody = !LOGGER_REQ_RES_LOG_BODY_ENABLED ? {} : reqResBodyLog ? body : {};
      return {
        timestamp,
        url: requestUrl,
        method,
        httpVersion,
        ipAddress: requestIp,
        userAgent,
        reqBody
      };
    }

    getResponseProps(request, response) {
      var timestamp = Date.now();
      var {
        body = {}
      } = response;
      var statusCode = body.statusCode || response.statusCode;
      var status = body.status || response.statusMessage;
      var responseMessage = body.message;
      var responseTimeInMS = request.timestamp && timestamp - request.timestamp || -1;
      var reqResBodyLog = httpContext.getReqResBodyLog();
      var resBody = !LOGGER_REQ_RES_LOG_BODY_ENABLED ? {} : reqResBodyLog ? body : {};
      return {
        responseTimeInMS,
        statusCode,
        status,
        responseMessage,
        resBody
      };
    }

  };
}