'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var CODE_FLOW_LOG = 'CODE_FLOW';
var REQ_RES_LOG = 'REQ_RES_LOG';
var LEAN_MAP = {
  [CODE_FLOW_LOG]: 'cf',
  [REQ_RES_LOG]: 'rr'
};
var LOG_TYPES = {
  CODE_FLOW_LOG,
  REQ_RES_LOG,
  LEAN_MAP
};
var _default = LOG_TYPES;
exports.default = _default;