'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var LEVELS = {
  FATAL: 'FATAL',
  ERROR: 'ERROR',
  WARN: 'WARN',
  INFO: 'INFO',
  DEBUG: 'DEBUG',
  TRACE: 'TRACE'
};
var LEVEL_RANK = {
  [LEVELS.FATAL]: 0,
  [LEVELS.ERROR]: 1,
  [LEVELS.WARN]: 2,
  [LEVELS.INFO]: 3,
  [LEVELS.DEBUG]: 4,
  [LEVELS.TRACE]: 5
};
var LEVEL_COLORS = {
  [LEVELS.FATAL]: 'magentaBright',
  [LEVELS.ERROR]: 'redBright',
  [LEVELS.WARN]: 'yellow',
  [LEVELS.INFO]: 'greenBright',
  [LEVELS.DEBUG]: 'blueBright',
  [LEVELS.TRACE]: undefined
};
var MODES = {
  CONSOLE: 'CONSOLE',
  CONSOLE_VERBOSE: 'CONSOLE_VERBOSE'
};
var LOGGER = {
  LEVELS,
  LEVEL_RANK,
  LEVEL_COLORS,
  MODES
};
var _default = LOGGER;
exports.default = _default;